/**
  This is the ESP32 version of my CMRI-enabled remote node processor.

  Combined with the ESP32_RemoteNode sketch, it provides I/O pins without running
  large amount of wiring all over your layout.  You can have nodes at each
  interlocking transmitting back switch position and occupancy information,
  and setting signal aspects, all powered either from a 12v bus, or even
  rechargable batteries.

  This should run on any ESP32 module.  It has been tested on an EZSBC development board.

  Use ESP32 Dev Module

  TODO: Make sure no output is 'live' until we get a sync from the BigNode
  TODO: I2C display to show bit info using glyphs?

  Named mesh here: https://gitlab.com/painlessMesh/painlessMesh/-/tree/master/examples/namedMesh

*/

// PainlessMesh version 1.3.0
//#include <painlessMesh.h>
#include "namedMesh.h"
// ArduinoJSON needs to be v 5.13.2 for painlessMesh 1.3.0
//#include <ArduinoJson.h>
#include <Pushbutton.h>
#include <EEPROM.h>
#include <U8x8lib.h>

#define   MESH_SSID       "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

//  Are the outputs high or low to indicate 'on' state?
#define   ON_HIGH_OR_LOW  0

#define DEBUG
#define VERSION "0.9"

// Prototypes
void receivedCallback(uint32_t from, String & msg);
void newConnectionCallback(uint32_t nodeId);
void changedConnectionCallback();
void nodeTimeAdjustedCallback(int32_t offset);
void delayReceivedCallback(uint32_t from, int32_t delay);

Scheduler     userScheduler; // to control your personal task
//painlessMesh  mesh;
namedMesh mesh;
SimpleList<uint32_t> nodes;

void pingRootNode();
Task taskPingRootNode( 3000, TASK_FOREVER, &pingRootNode ); // three second interval

bool onFlag = false;      //  Flag for alternating indicator
bool bConnected = false;  //  Are we connected to the mesh?

//  Our Node ID/Name
int g_nodeId;
String nodeName = "Nodexxx";

//  6 inputs
boolean inputPin[6];
int inputPinMap[6] = {26, 25, 32, 33, 34, 35};

// 12 outputs
boolean outputPin[12];
int outputPinMap[12] = {4, 5, 12, 13, 14, 15, 16, 17, 18, 19, 23, 27};

Pushbutton buttonUp(2);
Pushbutton buttonDown(0);

U8X8_SSD1306_128X64_NONAME_SW_I2C display( /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);

void setup() {
  Serial.begin(115200);
  EEPROM.begin(1);

  // I/O pin housekeeping

  for (int i = 0; i < 6; i++) {
    inputPin[i] = false;
    pinMode(inputPinMap[i], INPUT_PULLUP);
  }

  for ( int i = 0; i < 12; i++ ) {
    outputPin[i] = false;
    pinMode(outputPinMap[i], OUTPUT);
  }

  display.begin();
  display.setFlipMode(1);
  display.setFont(u8x8_font_amstrad_cpc_extended_r);

  //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  //mesh.setDebugMsgTypes(ERROR | DEBUG | CONNECTION | COMMUNICATION);  // set before init() so that you can see startup messages
  //mesh.setDebugMsgTypes(ERROR | DEBUG | CONNECTION);  // set before init() so that you can see startup messages
  //mesh.setDebugMsgTypes( CONNECTION | SYNC );

  mesh.init(MESH_SSID, MESH_PASSWORD, &userScheduler, MESH_PORT);
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
  mesh.onNodeDelayReceived(&delayReceivedCallback);

  //mesh.onReceive([](uint32_t from, String &msg) {
  //  Serial.printf("11 Received message by id from: %u, %s\n", from, msg.c_str());
  //});
  //mesh.onReceive([](String &from, String &msg) {
  //  Serial.printf("22 Received message by name from: %s, %s\n", from.c_str(), msg.c_str());
  //});

  setNodeIdAndName();

  userScheduler.addTask( taskPingRootNode );
  taskPingRootNode.enable();

  randomSeed(analogRead(A0));

  //  Turn off all output pins
  for ( int i = 0; i < 12; i++ ) {
    outputPin[i] = ON_HIGH_OR_LOW == 0 ? true : false;
  }
}

void loop() {
  userScheduler.execute(); // it will run mesh scheduler as well
  mesh.update();

  if ( buttonDown.getSingleDebouncedPress() )
  {
    if ( g_nodeId > 0 ) {
      g_nodeId = g_nodeId - 1;
      EEPROM.write(0, g_nodeId);
      EEPROM.commit();
      setNodeIdAndName();
    }
  }
  if (buttonUp.getSingleDebouncedPress())
  {
    if ( g_nodeId < 253 ) {
      g_nodeId = g_nodeId + 1;
      EEPROM.write(0, g_nodeId);
      EEPROM.commit();
      setNodeIdAndName();
    }
  }
  setOutputPins();
  setInputPins();
}

void pingRootNode() {
  // {from: "Node0", bits: '100101'}
  String to = "RootNode";
  String msg;
  String pins;
  for ( int i = 0; i < 6; i++ ) {
    pins += inputPin[i] ? "1" : "0";
  }

  StaticJsonDocument<200> doc;
  doc["node"] = nodeName;
  doc["pins"] = pins;
  serializeJson(doc, msg);

  mesh.sendSingle(to, msg);
  //Serial.println(msg);
  updateDisplay();
}

void setNodeIdAndName() {
  //  Set node name to ID from EEPROM
  int g_nodeId = EEPROM.read(0);
  if ( g_nodeId == 255 ) {
    g_nodeId = 0;
    EEPROM.write(0, g_nodeId);
    EEPROM.commit();
  }
  nodeName = "Node";
  nodeName += g_nodeId;
  mesh.setName(nodeName);
}

void updateDisplay() {
  display.drawString(0, 0, "RemoteNode v");
  display.drawString(12, 0, VERSION);
  display.draw1x2String(1, 3, "Node #");
  display.draw1x2String(8, 3, "    ");
  display.draw1x2String(8, 3, nodeName.substring(4).c_str());
  if ( onFlag == true ) {
    display.drawString(14, 3, "X");
    display.drawString(14, 4, " ");
  } else {
    display.drawString(14, 3, " ");
    display.drawString(14, 4, "X");
  }
  // display pin status
  for ( int i = 0; i < 12; i++ ) {
    display.drawString(i, 6, outputPin[i] ? "x" : " ");
  }
  for ( int i = 0; i < 6; i++ ) {
    display.drawString(i, 7, inputPin[i] ? "x" : " ");
  }

  nodes = mesh.getNodeList();
  char txt[16];
  sprintf(txt, "%02d", nodes.size());
  display.drawString(14, 7, txt);

  onFlag = !onFlag;
}

void setOutputPins() {
  //  Take ON_HIGH_OR_LOW into account here
  for ( int i = 0; i < 12; i++ ) {
    digitalWrite(outputPinMap[i], outputPin[i]);
  }
}

void setInputPins() {
  for ( int i = 0; i < 6; i++ ) {
    inputPin[i] = !digitalRead(inputPinMap[i]);
  }
}

// Callbacks:

void receivedCallback(uint32_t from, String & msg) {
  Serial.printf("Received from %u msg=%s\n", from, msg.c_str());
  StaticJsonDocument<200> doc;
  DeserializationError error = deserializeJson(doc, msg);
  String pins = doc["pins"];
  outputPin[0] = pins.charAt(0) == '0';
  outputPin[1] = pins.charAt(1) == '0';
  outputPin[2] = pins.charAt(2) == '0';
  outputPin[3] = pins.charAt(3) == '0';
  outputPin[4] = pins.charAt(4) == '0';
  outputPin[5] = pins.charAt(5) == '0';
  outputPin[6] = pins.charAt(6) == '0';
  outputPin[7] = pins.charAt(7) == '0';
  outputPin[8] = pins.charAt(8) == '0';
  outputPin[9] = pins.charAt(9) == '0';
  outputPin[10] = pins.charAt(10) == '0';
  outputPin[11] = pins.charAt(11) == '0';
}

void newConnectionCallback(uint32_t nodeId) {
  onFlag = false;
  bConnected = true;

  Serial.printf("New Connection, nodeId = %u\n", nodeId);

  nodes = mesh.getNodeList();

  Serial.printf("Num nodes: %d\n", nodes.size());
  Serial.printf("Connection list:");

  SimpleList<uint32_t>::iterator node = nodes.begin();
  while (node != nodes.end()) {
    Serial.printf(" %u", *node);
    node++;
  }
  Serial.println();

}

void changedConnectionCallback() {
  Serial.printf("Changed connections %s\n", mesh.subConnectionJson().c_str());
  onFlag = false;
  bConnected = true;

  nodes = mesh.getNodeList();

  Serial.printf("Num nodes: %d\n", nodes.size());
  Serial.printf("Connection list:");

  SimpleList<uint32_t>::iterator node = nodes.begin();
  while (node != nodes.end()) {
    Serial.printf(" %u", *node);
    node++;
  }
  Serial.println();
}

void nodeTimeAdjustedCallback(int32_t offset) {
  //Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}

void delayReceivedCallback(uint32_t from, int32_t delay) {
  //Serial.printf("Delay to node %u is %d us\n", from, delay);
}
