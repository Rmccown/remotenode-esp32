# RemoteNode for ESP32

This is the companion remote node project for my [BigNode](https://gitlab.com/Rmccown/bignode-esp32
). You can read about the project there.  This README will address the Remote Node only.

# Getting Started

## What to buy

If you intend on building using my Gerber files, you will need the following components for each node:
* ESP32 Development Board from [EZSBC](https://www.ezsbc.com/)
* Sockets (2.54 pitch, 20 pin)
* .96" I2C Display
* 6x6x5mm 2 Pin PCB Momentary DIP Push Buttons
* Spade connectors

## Prerequisites

* Arduino IDE 1.8.13
* [painlessMesh](https://gitlab.com/painlessMesh/painlessMesh), and the [namedMesh.h](https://platformio.org/lib/show/1269/painlessMesh) file from the examples.

## Setup

Edit the script.  Change the following settings to reflect your reality:

| Variable | Comment |
| ------ | ------ |
| MESH_SSID | SSID for Bignode network |
| MESH_PASSWORD | Password for Bignode network | 
| MESH_PORT | Random number here |
| DEBUG | Setting this to `1` will cause a lot of output in the serial monitor |

Note that MESH_SSID, MESH_PASSWORD, and MESH_PORT need to be the same for all nodes (BigNode and Remotes) in a network.

Compile and load this into your ESP32 module.

# Roadmap

Things to do on the journey to v1.0

* Finish production Gerber files
* Indicator that the node is connected to the mesh network
